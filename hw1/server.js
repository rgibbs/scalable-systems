var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'nintendo4545R',
  database: 'hw1'
});

connection.connect();

var clearSql = 'TRUNCATE TABLE ??';
var clearInserts = ['activeSessions'];
clearSql = mysql.format(clearSql,clearInserts);
connection.query(clearSql,function(err){});


var app = express();
app.use(bodyParser.json());

///////////
// LOGIN //
///////////

// This responds to a POST request for the login page
app.post('/login', function (req, res) {
  console.log("Got a POST request for the login page");

  // console.log("Username is: " + req.body.username);
  // console.log("Password is: " + req.body.password);

  var validUser = false;

  var sql = 'SELECT * FROM ?? WHERE ?? = ?';
  var inserts = ['users','username',req.body.username];
  sql = mysql.format(sql,inserts);
  // console.log("sql is: " + sql);



  connection.query(sql,function(err,rows,fields) {

    if(err) throw err;

      //if username is in database
      if(rows.length == 1) {
        // console.log("username matches");
        //if password matches too
        if(rows[0].password == req.body.password) {
          // console.log("password matches");
          validUser = true;

          //figure out if current session is still working

          var sessionCheckSql = 'SELECT * FROM ?? WHERE ?? = ?';
          var insertCheck = ['activeSessions', 'client_ip', req.ip];
          sessionCheckSql = mysql.format(sessionCheckSql,insertCheck);
          // console.log("sessionCheckSql is: " + req.ip)

          connection.query(sessionCheckSql,function(checkErr,checkRows,checkFields){
            //if no entry in the table that matches, 
            //this is a new session, simply add
            if(checkRows.length == 0) {
              var sessionAddSql = 'INSERT INTO ?? VALUES(?,?,NOW())';
              var insertAdd = ['activeSessions', req.body.username, req.ip];
              sessionAddSql = mysql.format(sessionAddSql,insertAdd);
              // console.log("sessionAddSql is: " + sessionAddSql);

              connection.query(sessionAddSql,function(addErr) {});
            }
            //if entry is already in table, update session
            else {
              var sessionUpdateSql = 'UPDATE ?? SET ?? = ?, ?? = NOW() WHERE ?? = ?';
              var insertUpdate = ['activeSessions', 'username', req.body.username, 'timestamp', 'client_ip', req.ip];
              sessionUpdateSql = mysql.format(sessionUpdateSql,insertUpdate);
              // console.log("sessionUpdateSql is: " + sessionUpdateSql);

              connection.query(sessionUpdateSql,function(updateErr) {});
            }
          });

        }
        
      }

      //should've returned above here if successfull
      // console.log("validUser is: " + validUser);
      if (validUser) {
        res.json({message : "Welcome " + rows[0].firstname});
      } else {
        res.json({message : "There seems to be an issue with the username/password combination that you entered"})
      }

    //connection.end();
  });
});

////////////
// LOGOUT //
////////////

// This responds to a POST request for the logout page
app.post('/logout', function (req, res) {
  console.log("Got a POST request for the logout page");


  var sessionCheckSql = 'SELECT * FROM ?? WHERE ?? = ?';
  var insertCheck = ['activeSessions', 'client_ip', req.ip];
  sessionCheckSql = mysql.format(sessionCheckSql,insertCheck);

  connection.query(sessionCheckSql,function(checkErr,checkRows,checkFields){
    //if no entry in the table that matches, respond not logged in  
    if(checkRows.length == 0) {
      res.json({message : "You are not currently logged in"});
    }
    //if entry is already in table, logout
    else {
      var sessionDeleteSql = 'DELETE FROM ?? WHERE ?? = ?';
      var insertDelete = ['activeSessions', 'client_ip', req.ip];
      sessionDeleteSql = mysql.format(sessionDeleteSql,insertDelete);
      // console.log("sessionDeleteSql is: " + sessionDeleteSql);

      connection.query(sessionDeleteSql,function(updateErr) {});
      res.json({message : "You have been successfully logged out"});
    }
  });
});

/////////
// ADD //
/////////

// This responds to a POST request for the add page
app.post('/add', function (req, res) {
  console.log("Got a POST request for the add page");

  //check if this ip is currently logged in
  var sessionCheckSql = 'SELECT TIMESTAMPDIFF(minute,??,NOW()) AS ?? FROM ?? WHERE ?? = ?';
  var insertCheck = ['timestamp', 'deltaTime', 'activeSessions', 'client_ip', req.ip];
  sessionCheckSql = mysql.format(sessionCheckSql,insertCheck);
  //console.log("SessionChecksql is: " + sessionCheckSql);

  connection.query(sessionCheckSql,function(checkErr,checkRows,checkFields){
    //if no entry in the table that matches, 
    //respond not logged in  
    if(checkRows.length == 0) {
      res.json({message : "You are not currently logged in"});
      return;
    }
    //if entry is already in table
    //check if time is still valid or need to trigger logout
    else if (checkRows[0].deltaTime >= 15) {
      console.log("Session timed out.");
      var sessionDeleteSql = 'DELETE FROM ?? WHERE ?? = ?';
      var insertDelete = ['activeSessions', 'client_ip', req.ip];
      sessionDeleteSql = mysql.format(sessionDeleteSql,insertDelete);
      // console.log("sessionDeleteSql is: " + sessionDeleteSql);
      connection.query(sessionDeleteSql,function(updateErr) {});
      res.json({message : "You are not currently logged in"});
      return;
    }
    // update session and do math
    else {
      //console.log("checkRows[0].timestampdiff is: " + checkRows[0].);

      var sessionUpdateSql = 'UPDATE ?? SET ?? = NOW() WHERE ?? = ?';
      var insertUpdate = ['activeSessions', 'timestamp', 'client_ip', req.ip];
      sessionUpdateSql = mysql.format(sessionUpdateSql,insertUpdate);
      // console.log("sessionUpdateSql is: " + sessionUpdateSql);

      connection.query(sessionUpdateSql,function(updateErr) {});

      //make sure num1 and num2 are numbers
      if(isNaN(req.body.num1) || isNaN(req.body.num2)) {
        res.json({message : "The numbers you entered are not valid"});
        return;
      }
      // console.log("num1 is: " + req.body.num1);
      // console.log("num2 is: " + req.body.num2);

      var sum = Number(req.body.num1) + Number(req.body.num2);
      // console.log("sum is: " + sum);
      res.json({message : "The action was successful", result: sum});

      
    }
  });

});

////////////
// DIVIDE //
////////////

// This responds to a POST request for the divide page
app.post('/divide', function (req, res) {
  console.log("Got a POST request for the divide page");

  //check if this ip is currently logged in
  var sessionCheckSql = 'SELECT TIMESTAMPDIFF(minute,??,NOW()) AS ?? FROM ?? WHERE ?? = ?';
  var insertCheck = ['timestamp', 'deltaTime', 'activeSessions', 'client_ip', req.ip];
  sessionCheckSql = mysql.format(sessionCheckSql,insertCheck);

  connection.query(sessionCheckSql,function(checkErr,checkRows,checkFields){
    //if no entry in the table that matches, 
    //respond not logged in  
    if(checkRows.length == 0) {
      res.json({message : "You are not currently logged in"});
      return;
    }
    //if entry is already in table
    //check if time is still valid or need to trigger logout
    else if (checkRows[0].deltaTime >= 15) {
      var sessionDeleteSql = 'DELETE FROM ?? WHERE ?? = ?';
      var insertDelete = ['activeSessions', 'client_ip', req.ip];
      sessionDeleteSql = mysql.format(sessionDeleteSql,insertDelete);
      // console.log("sessionDeleteSql is: " + sessionDeleteSql);
      connection.query(sessionDeleteSql,function(updateErr) {});
      res.json({message : "You are not currently logged in"});
      return;
    }
    // update session and do math
    else {
      var sessionUpdateSql = 'UPDATE ?? SET ?? = NOW() WHERE ?? = ?';
      var insertUpdate = ['activeSessions', 'timestamp', 'client_ip', req.ip];
      sessionUpdateSql = mysql.format(sessionUpdateSql,insertUpdate);
      // console.log("sessionUpdateSql is: " + sessionUpdateSql);

      connection.query(sessionUpdateSql,function(updateErr) {});

      //make sure num1 and num2 are numbers
      if(isNaN(req.body.num1) || isNaN(req.body.num2)) {
        res.json({message : "The numbers you entered are not valid"});
        return;
      }
      // console.log("num1 is: " + req.body.num1);
      // console.log("num2 is: " + req.body.num2);

      var divided = Number(req.body.num1) / Number(req.body.num2);
      // console.log("divided is: " + divided);
      res.json({message : "The action was successful", result: divided});

      
    }
  });

});

//////////////
// MULTIPLY //
//////////////

// This responds to a POST request for the multiply page
app.post('/multiply', function (req, res) {
  console.log("Got a POST request for the multiply page");

  //check if this ip is currently logged in
  var sessionCheckSql = 'SELECT TIMESTAMPDIFF(minute,??,NOW()) AS ?? FROM ?? WHERE ?? = ?';
  var insertCheck = ['timestamp', 'deltaTime', 'activeSessions', 'client_ip', req.ip];
  sessionCheckSql = mysql.format(sessionCheckSql,insertCheck);

  connection.query(sessionCheckSql,function(checkErr,checkRows,checkFields){
    //if no entry in the table that matches, 
    //respond not logged in  
    if(checkRows.length == 0) {
      res.json({message : "You are not currently logged in"});
      return;
    }
    //if entry is already in table
    //check if time is still valid or need to trigger logout
    else if (checkRows[0].deltaTime >= 15) {
      var sessionDeleteSql = 'DELETE FROM ?? WHERE ?? = ?';
      var insertDelete = ['activeSessions', 'client_ip', req.ip];
      sessionDeleteSql = mysql.format(sessionDeleteSql,insertDelete);
      // console.log("sessionDeleteSql is: " + sessionDeleteSql);
      connection.query(sessionDeleteSql,function(updateErr) {});
      res.json({message : "You are not currently logged in"});
      return;
    }
    // update session and do math
    else {
      var sessionUpdateSql = 'UPDATE ?? SET ?? = NOW() WHERE ?? = ?';
      var insertUpdate = ['activeSessions', 'timestamp', 'client_ip', req.ip];
      sessionUpdateSql = mysql.format(sessionUpdateSql,insertUpdate);
      // console.log("sessionUpdateSql is: " + sessionUpdateSql);

      connection.query(sessionUpdateSql,function(updateErr) {});

      //make sure num1 and num2 are numbers
      if(isNaN(req.body.num1) || isNaN(req.body.num2)) {
        res.json({message : "The numbers you entered are not valid"});
        return;
      }
      // console.log("num1 is: " + req.body.num1);
      // console.log("num2 is: " + req.body.num2);

      var multiplied = Number(req.body.num1) * Number(req.body.num2);
      // console.log("multiplied is: " + multiplied);
      res.json({message : "The action was successful", result: multiplied});

      
    }
  });

});

var server = app.listen(3000, '0.0.0.0', function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Server app listening at http://%s:%s", host, port)

});
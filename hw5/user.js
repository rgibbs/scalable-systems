var helper = require('./helper');
var AWS = require('aws-sdk');
var mysql = require('mysql');
var async = require('async');
var uuidV4 = require('uuid/v4');


exports.registerUser  = function registerUser(req, res, dynamodb) {
  // console.log("Got a POST request for the registration page");
  //console.log(pool);

  var registerAdmin = false;

  //check if all fields valid, FAIL FAST!
  if(helper.isEmptyString(req.body.fname)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmptyString(req.body.lname)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmptyString(req.body.address)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmptyString(req.body.city)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmptyString(req.body.state)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmptyString(req.body.zip)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmptyString(req.body.email)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmptyString(req.body.username)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmptyString(req.body.password)) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  var docClient = new AWS.DynamoDB.DocumentClient(dynamodb);

  var userInfo = {
    TableName:"users",
    Item:{
        "username": req.body.username,
        "fname": req.body.fname,
        "lname": req.body.lname,
        "password": req.body.password,
        "email": req.body.email,
        "address": req.body.address,
        "city": req.body.city,
        "state": req.body.state,
        "zip": req.body.zip,
        "admin": registerAdmin,
        "userID": uuidV4()
    },
    ReturnValues: 'NONE',
    //only register the user if there is not one with this username
    ConditionExpression: "attribute_not_exists(username)"
  };

  //console.log("Attempting to add a new item...");
  docClient.put(userInfo, function(err, data) {
    if (err) {
      //respond failure
      console.error("Unable to register user. Error JSON:", JSON.stringify(err, null, 2));  
      res.json({message : "The input you provided is not valid"});
      return;
    } else {
      //respond success
      //console.log("Added item:", JSON.stringify(data, null, 2));
      res.json({message : "The action was successful"});
    }
  });

};


exports.login = function login(req, res, dynamodb) {
  // console.log("Got a POST request for the login page");
  // console.log("Username is: " + req.body.username);
  // console.log("Password is: " + req.body.password);
  req.session.loggedIn = false;

  var docClient = new AWS.DynamoDB.DocumentClient(dynamodb);

  var loginInfo = {
    TableName:"users",
    Key:{
        "username": req.body.username,
    },
    AttributesToGet: [
      "fname",
      "admin",
      "userID"
    ],
    //only login the user if the username password combo matches
    ConditionExpression: "password = :p",
    ExpressionAttributeValues: {
        ":p": req.body.password
    }
  };

  // console.log("Attempting to login...");
  docClient.get(loginInfo, function(err, data) {
    if(err) {
      //respond failure
      console.error("Unable to login. Error JSON:", JSON.stringify(err, null, 2));  
      res.json({message : "There seems to be an issue with the username/password combination that you entered"});
      return;
    } else if(helper.isEmptyJSON(data)) {
      // console.log("Unable to login. No username/password match.");
      res.json({message : "There seems to be an issue with the username/password combination that you entered"});
      return;
    } else {
      // console.log("Logging in...");
      // console.log("data: " + JSON.stringify(data, null, 2));
      // console.log("data.Item: " + data.Item);
      // console.log("data.Item.fname: " + data.Item.fname);

      req.session.loggedIn = true;
      req.session.userID = data.Item.userID;
      req.session.username = req.body.username;
      req.session.admin = data.Item.admin;
      res.json({message : "Welcome " + data.Item.fname});
    }
  });
}

exports.logout = function logout(req, res, dynamodb) {
  // console.log("Got a POST request for the logout page");
  if(req.session.loggedIn) {
    // console.log("Logging out...");
    req.session.loggedIn = false;
    res.json({message : "You have been successfully logged out"});
  } else {
    // console.log("Not logged in, unable to logout.")
    res.json({message : "You are not currently logged in"});
  }
}

exports.updateInfo = function updateInfo(req,res,dynamodb) {

  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  //check which keys are present
  var updateExpression = "set ";
  var updateKeys = [];
  var updateKeyPlaceholders = [];
  var updateValues = [];
  var updateValuePlaceholders = [];

  var counter = 0;
  var usernameChangeRequested = false;
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!helper.isEmptyString(req.body[key])){
        counter++;
        updateExpression += ("#key"+counter+" = "+":val"+counter+",");
        updateKeys.push(key);
        updateKeyPlaceholders.push("#key"+counter);
        updateValues.push(req.body[key]);
        updateValuePlaceholders.push(":val"+counter);
      }
    }
  }

  //if no values to update
  if(counter == 0) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  //toggle if requesting a username change
  if(!helper.isEmptyString(req.body.username)) {
    usernameChangeRequested = true;
  }

  var docClient = new AWS.DynamoDB.DocumentClient(dynamodb);

  //trim trailing comma
  updateExpression = updateExpression.substring(0,updateExpression.length-1);
  
  // console.log("updateExpression: " + updateExpression);
  // console.log("updateKeys: " + updateKeys);
  // console.log("updateValues: " + updateValues);

  var expressionNames = {};
  var expressionValues = {};

  for (var i = 0; i < updateKeys.length; i++) {
    expressionNames[updateKeyPlaceholders[i]] = updateKeys[i];
    expressionValues[updateValuePlaceholders[i]] = updateValues[i];
  }

  //can just update existing item
  if (!usernameChangeRequested) {
    var updatedUserInfo = {
      TableName:"users",
      Key:{
          "username": req.session.username,
      },
      UpdateExpression: updateExpression,
      ExpressionAttributeNames: expressionNames,
      ExpressionAttributeValues: expressionValues,
      ReturnValues:"NONE"
    };

    // console.log("updatedUserInfo: " + updatedUserInfo);
    // console.log("JSON.stringify(updatedUserInfo): " + JSON.stringify(updatedUserInfo));

    docClient.update(updatedUserInfo, function(err, data) {
      if (err)  {
        console.log(err);
        console.log("Dynamo Error. Not updating item.");
        res.json({message : "The input you provided is not valid"});
        return;
      } else {
        // console.log(data);
        // console.log("Update successful.");
        res.json({message : "The action was successful"});
        return;
      }
    });

  } else {
  //need to copy contents of existing item to new item with updates
  //and delete existing item

    //get existing item
    var userInfo = {
      TableName:"users",
      Key:{
        //use exisiting username not the new one
        "username": req.session.username,
      }
    };

    // console.log("Getting current user info...");
    docClient.get(userInfo, function(err, data) {
      if(err) {
        //respond failure
        console.error("Unable to update. Error JSON:", JSON.stringify(err, null, 2));  
        res.json({message : "The input you provided is not valid"});
        return;
      } else if(helper.isEmptyJSON(data)) {
        // console.log("Unable to find info for this username");
        res.json({message : "The input you provided is not valid"});
        return;
      } else {

        // console.log("oldItem: " + JSON.stringify(data.Item));

        //data acquired, create new copy and delete current
        async.series([
          function createNewUser(callback){

            var newItem = {};

            for (var i in data.Item) {
              if(req.body.hasOwnProperty(i)) {
                newItem[i] = req.body[i];
              } else {
                newItem[i] = data.Item[i];
              }
            }

            // console.log("New item: " + JSON.stringify(newItem));

            var newUserInfo = {
              TableName:"users",
              Item: newItem,
              ReturnValues: 'NONE',
              //only register the user if there is not one with this username
              ConditionExpression: "attribute_not_exists(username)"
            };

            //console.log("Attempting to add a new item...");
            docClient.put(newUserInfo, function(err, data) {
              if (err) {
                //respond failure
                console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));  
                callback("UNABLE TO ADD COPY");
              } else {
                //respond success
                //console.log("Added item:", JSON.stringify(data, null, 2));
                callback();
              }
            });
          },
          function deleteOldUser(callback) {

            var deleteParams = {
                TableName:"users",
                Key:{
                    "username": req.session.username
                }
            };

            // console.log("Attempting to delete old user item...");
            docClient.delete(deleteParams, function(err, data) {
                if (err) {
                  console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
                  callback("UNABLE TO DELETE OLD ITEM");
                } else {
                  // console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
                  callback();
                }
            });
          }],
          //handle errors or send response
          function(err,results){
            // console.log("final callback");
            if(err == "UNABLE TO ADD COPY") {
              //respond failure
              console.log(err);
              res.json({message : "There are no users that match that criteria"});
              return;
            } else if(err == "UNABLE TO DELETE OLD ITEM") {
              //respond failure
              console.log(err);
              res.json({message : "There are no users that match that criteria"});
              return;
            } else {
              //respond success
              // console.log("Update user successful");
              req.session.username = req.body.username;
              res.json({message: "The action was successful"});
            }
          }
        );
      }
    });
  }
}  

exports.viewUsers = function viewUsers(req,res,dynamodb) {

  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  if(!req.session.admin) {
    res.json({message : "You must be an admin to perform this action"});
    return;
  }

  //check which keys are present
  var viewKeys = [];
  var viewKeyPlaceholders = [];
  var viewValues = [];
  var viewValuePlaceholders = [];
  var viewFilterExpression = "begins_with(";

  var counter = 0;
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!helper.isEmptyString(req.body[key])){
        counter++;
        viewFilterExpression += ("#key"+counter+","+":val"+counter+") AND begins_with(");
        viewKeys.push(key);
        viewKeyPlaceholders.push("#key"+counter);
        viewValues.push(req.body[key]);
        viewValuePlaceholders.push(":val"+counter);
      }
    }
  }

  if(counter == 0) {
    var usersFilter = {
      TableName: "users",
      ProjectionExpression: "fname, lname, userID",
    };
  } else {

    //trim trailing ' AND begins_with('
    viewFilterExpression = viewFilterExpression.substring(0,viewFilterExpression.length-17);

    var filterExpressionNames = {};
    var filterExpressionValues = {};

    for (var i = 0; i < viewKeys.length; i++) {
      filterExpressionNames[viewKeyPlaceholders[i]] = viewKeys[i];
      filterExpressionValues[viewValuePlaceholders[i]] = viewValues[i];
    }

    var usersFilter = {
      TableName: "users",
      ProjectionExpression: "fname, lname, userID",
      FilterExpression: viewFilterExpression,
      ExpressionAttributeNames: filterExpressionNames,
      ExpressionAttributeValues: filterExpressionValues
    };
  }

  // console.log("Scanning users table.");

  var matchingUsers = [];

  var docClient = new AWS.DynamoDB.DocumentClient(dynamodb);

  docClient.scan(usersFilter, onScan);
  function onScan(err, data) {
      if (err) {
          console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
          //pack all the found users into the final JSON
          // console.log("Scan succeeded.");
          for (var i = 0; i < data.Items.length; i++) {
            matchingUsers.push(data.Items[i]);
          }

          // continue scanning if we have more movies, because
          // scan can retrieve a maximum of 1MB of data
          if (typeof data.LastEvaluatedKey != "undefined") {
              // console.log("Scanning for more...");
              usersFilter.ExclusiveStartKey = data.LastEvaluatedKey;
              docClient.scan(usersFilter, onScan);
          } else {
            //done scanning so we can submit the final json now
            // console.log("Users found: " + JSON.stringify(matchingUsers));

            if(helper.isEmptyJSON(matchingUsers)) {
              res.json({message: "There are no users that match that criteria"});
            } else {
              res.json({
                message: "The action was successful",
                user:matchingUsers
              });
            }  
          }
      }
  }

}

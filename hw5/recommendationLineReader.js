/*
lineReader will extract the records from amazon-meta.txt one at a time as
file is too large to read all at once.  In order to add records to a database you need to add code below to insert records

This code depnds on "line-reader"

You need to install line-reader by using the following command:
npm install line-reader

*/

//This assumes that you're using mysql.  You'll need to change this if you're using another database
var helper = require('./helper');
var AWS = require('aws-sdk');
var https = require('https');
var query;
var jsonRecord;
var execute = true;
var query = "";
var totalRecords = 0;
var fs = require('fs');
var args = process.argv.slice(2);
var fileName = args[0];
console.log("Processing lines for:" + fileName)
//var fileName = "productRecords.json";


var lineReader = require('line-reader');


//You need to change this to be appropriate for your system
AWS.config.loadFromPath('./awsConfig.json');
var agent = new https.Agent({
   maxSockets: 25
});
AWS.config.update({
   httpOptions:{
      agent: agent
   }
});

var dynamodb = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient(dynamodb);


var values = ""; //The records read from the file.
var numRecords = 0; //The current number of records read from the file.
var recordBlock = 25; //The number of records to write at once.
var test;
var maxDescriptionLength = 0;
var maxTitleLength = 0;
lineReader.eachLine(fileName, function(line, last) {
  //console.log("each line");
  execute = false;
  currentLine = line.toString();
  try{
    //test = "Haven't touched values yet."
    //jsonRecord = JSON.parse(currentLine);
    //console.log("jsonRecord: " + JSON.stringify(jsonRecord));
    //console.log("currentLine: " + currentLine);

    //trim trailing commas, if any
    currentLine = currentLine.split(',');

    //console.log("currentLine.length: " + currentLine.length);
    var lineArray = [];
    //check for empty values
    for (var i = 0;i < currentLine.length;i++) {
      if(!helper.isEmptyString(currentLine[i])) {
        lineArray.push(currentLine[i]);
      }
    }

    //console.log("lineArray: " + lineArray);

    if(lineArray.length > 1) { 
      //update recommendations


      for (var j = 0;j<lineArray.length;j++) {
        var recommendExpression = "add ";
        var recommendNames = {};
        counter = 0;

        for (var i = 0; i < lineArray.length; i++) {
          if(i != j) {
            counter++;
            recommendExpression += ("#key"+counter+" :one,")
            recommendNames["#key"+counter] = lineArray[i];
          }
        }
        //trim trailing comma
        recommendExpression = recommendExpression.substring(0,recommendExpression.length-1);
        var recommendValues = {
          ":one":1
        };

        recommendationInfo = {
          TableName:"recommendations",
          Key:{
            "asin": lineArray[j]
          },
          UpdateExpression: recommendExpression,
          ExpressionAttributeNames: recommendNames,
          ExpressionAttributeValues: recommendValues,
          ReturnValues:"NONE"
        };

        docClient.update(recommendationInfo, function(err, data) {
          if (err)  {
            console.log(err);
            console.log("Dynamo Error. Not updating recommendation info.");
          } else {
            numRecords++;
            if(numRecords % 1000 == 0 || last) {
              console.log(numRecords + " processed successfully.");
            }
            // console.log(data);
            // console.log("Recommendation"+i+" update successful.");
          }
        });

      }
    //console.log("recommendationInfo: " + JSON.stringify(recommendationInfo));
    //console.log("recommendationInfo[0]: " + JSON.stringify(recommendationInfo[0]));
    }

  }catch(err) {
    //execute = false;//there was a quote in the text and the parse failed ... skip insert
    console.log(err);
    //console.log(test);
  }

  // if(execute){//***************Need to put logic to insert into your db*****************************************
  //   co(function* () {
  //       var resp = yield sql.query(query);
  //       //console.log(mysql.format(query))
  //       //connection.query(mysql.format(query));

  //       totalRecords += recordBlock;
  //       console.log(totalRecords + " records inserted.");

  //       if(last) {
  //         process.exit();
  //       }
  //   });
  // }//if(execute)
});//lineReader.eachLine

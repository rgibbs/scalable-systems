var express = require('express');
var router = express.Router();
var session = require('express-session');
var https = require('https');
var request = require('request');
var mysql = require('mysql');
var mysqlStore = require('express-mysql-session');
var async = require('async');
var bodyParser = require('body-parser');
var uid = require('uid-safe');
var ON_DEATH = require('death');
var AWS = require('aws-sdk');


var user = require('./user');
var product = require('./product');
var purchases = require('./purchases');
var helper = require('./helper');

var pool  = mysql.createPool({
  connectionLimit : 3,
  host: 'scalablesystemsmysql.cb9anwh1e9es.us-east-1.rds.amazonaws.com',
  port: '4545',
  user: 'rgibbs',
  password: 'abth4545R!',
  database: 'hw2'
});



AWS.config.loadFromPath('./awsConfig.json');
var agent = new https.Agent({
   maxSockets: 25
});
AWS.config.update({
   httpOptions:{
      agent: agent
   }
});

var dynamodb = new AWS.DynamoDB();

var options = {
    // Name of the table you would like to use for sessions.
    // Defaults to 'sessions'
    table: 'sessions',

    // Optional path to AWS credentials (loads credentials from environment variables by default)
    AWSConfigPath: './awsConfig.json',

    // Optional JSON object of AWS configuration options
    // AWSConfigJSON: {
    //     region: 'us-east-1',
    //     correctClockSkew: true
    // }

    // Optional. How often expired sessions should be cleaned up.
    // Defaults to 600000 (10 minutes).
    reapInterval: 600000
};


var DynamoDBStore = require('connect-dynamodb')({session: session});
//app.use(session({ store: new DynamoDBStore(options), secret: 'darkLightning', resave: true, saveUninitialized: true}));

var sessionStore = new DynamoDBStore(options);


// pool.getConnection(function(err, connection) {

//   clearSql = 'TRUNCATE TABLE ??';
//   clearInserts = ['users'];
//   clearSql = mysql.format(clearSql,clearInserts);
//   connection.query(clearSql, helper.throwError);

//   clearSql = 'TRUNCATE TABLE ??';
//   clearInserts = ['sessions'];
//   clearSql = mysql.format(clearSql,clearInserts);
//   connection.query(clearSql, helper.throwError);

//   //ADD DEFAULT USERS
//   var defaultUsersSql = 'INSERT INTO ?? (??,??,??,??,??) VALUES(?,?,?,?,?)'
//   var defaultInserts = ['users','username','password','fname','lname', 'admin',
//   'jadmin','admin','Jenny','Admin', '1'];
//   defaultUsersSql = mysql.format(defaultUsersSql,defaultInserts);
//   connection.query(defaultUsersSql, helper.throwError);

//   connection.release();

// });

var app = express();
app.use(bodyParser.json());
app.use(session({
  saveUninitialized: true,
  key: 'session_cookie_name',
  secret: 'darkLightning',
  resave: true,
  cookie: {maxAge: 900000},
  store: sessionStore
}))

//called when process is killed (SIGINT,SIGTERM,SIGQUIT)
ON_DEATH(function(signal, err) {
  
  console.log("\nBOOM Headshot...");
  
  //cleanup code
  pool.end();

  console.log("Cleanup complete. Process dying NOW!!!")
  process.exit();

})


/////////////////////
// SUPPORTED PAGES //
/////////////////////

router.post('/registerUser', registerUserRequest);
router.post('/login', loginRequest);
router.post('/logout', logoutRequest);
router.post('/updateInfo', updateInfoRequest);
router.post('/viewUsers', viewUsersRequest);
router.post('/addProducts', addProductsRequest);
router.post('/modifyProduct', modifyProductRequest);
router.post('/viewProducts', viewProductsRequest);
router.post('/buyProducts', buyProductsRequest);
router.post('/productsPurchased',productsPurchasedRequest);
router.post('/getRecommendations',getRecommendationsRequest);
router.get('/health', helper.healthy);


app.use('/',router);


var server = app.listen(3000, '0.0.0.0', function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Server app listening at http://%s:%s", host, port)

});

function registerUserRequest(req, res) {user.registerUser(req,res,dynamodb)};
function loginRequest(req, res) {user.login(req,res,dynamodb)};
function logoutRequest(req, res) {user.logout(req,res,dynamodb)};
function updateInfoRequest(req,res) {user.updateInfo(req,res,dynamodb)};
function viewUsersRequest(req,res) {user.viewUsers(req,res,dynamodb)};
function addProductsRequest(req,res) {product.addProducts(req,res,pool)};
function modifyProductRequest(req,res) {product.modifyProduct(req,res,pool)};
function viewProductsRequest(req,res) {product.viewProducts(req,res,pool)};
function buyProductsRequest(req,res) {purchases.buyProducts(req,res,pool,dynamodb)};
function productsPurchasedRequest(req,res) {purchases.productsPurchased(req,res,dynamodb)};
function getRecommendationsRequest(req,res) {purchases.getRecommendations(req,res,dynamodb)};


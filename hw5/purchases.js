var helper = require('./helper');
var AWS = require('aws-sdk');
var mysql = require('mysql');
var async = require('async');

exports.buyProducts = function buyProducts(req,res,pool,dynamodb) {

  //check for login
  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  //build list for IN clause
  //format "asin1","asin2","asin3"

  var sql = "SELECT ??,?? FROM ?? WHERE ?? IN("
  var inserts = ['asin','productName','products','asin'];

  for (var i = 0; i < req.body.products.length; i++) {
    sql += "?,";
    inserts.push(req.body.products[i].asin);
  }

  //trim trailing comma and close paren
  sql = sql.substring(0,sql.length-1) + ")";

  // console.log("sql: " + sql);
  // console.log("inserts: " + inserts);


  sql = mysql.format(sql,inserts);

  // console.log("sql query: " + sql);

  pool.getConnection(function(err, connection) {
    connection.query(sql,function checkProducts(err,rows,fields) {
      connection.release();
      if(err) {
        console.log(err);
      }

      
      //if products are in database
      if(rows.length > 0) {
        // console.log("found " + rows.length + " row(s)");
        // console.log("rows: " + JSON.stringify(rows));

        var docClient = new AWS.DynamoDB.DocumentClient(dynamodb);

        var updateExpression = "add ";
        var expressionNames = {};
        var counter = 0;

        for (var i = 0; i < rows.length; i++) {
          counter++;
          updateExpression += ("#key"+counter+" :one,")
          //updateKeyPlaceholders.push("#key"+counter);
          //updateKeys.push(rows[i].asin);
          expressionNames["#key"+counter] = rows[i].productName;
        }
        var expressionValues = {
          ":one":1
        };

        //trim trailing comma
        updateExpression = updateExpression.substring(0,updateExpression.length-1);

        // console.log("updateExpression: " + updateExpression);
        // console.log("expressionNames: " + JSON.stringify(expressionNames));
        // console.log("expressionValues: " + JSON.stringify(expressionValues));
        // console.log("userID: " + req.session.userID);
        //
        //update userID purchase history
        var purchaseInfo = {
          TableName:"purchaseHistory",
          Key:{
            "userID": req.session.userID,
          },
          UpdateExpression: updateExpression,
          ExpressionAttributeNames: expressionNames,
          ExpressionAttributeValues: expressionValues,
          ReturnValues:"NONE"
        };

        if(rows.length > 1) { 
          //update recommendations
          var recommendationInfo = [];

          for (var j = 0;j<rows.length;j++) {
            var recommendExpression = "add ";
            var recommendNames = {};
            counter = 0;

            for (var i = 0; i < rows.length; i++) {
              if(i != j) {
                counter++;
                recommendExpression += ("#key"+counter+" :one,")
                recommendNames["#key"+counter] = rows[i].asin;
              }
            }
            //trim trailing comma
            recommendExpression = recommendExpression.substring(0,recommendExpression.length-1);
            var recommendValues = {
              ":one":1
            };

            recommendationInfo[j] = {
              TableName:"recommendations",
              Key:{
                "asin": rows[j].asin
              },
              UpdateExpression: recommendExpression,
              ExpressionAttributeNames: recommendNames,
              ExpressionAttributeValues: recommendValues,
              ReturnValues:"NONE"
            };
          }
          //console.log("recommendationInfo: " + JSON.stringify(recommendationInfo));
          //console.log("recommendationInfo[0]: " + JSON.stringify(recommendationInfo[0]));
        }

        async.parallel([
          function(callback){
            docClient.update(purchaseInfo, function(err, data) {
              if (err)  {
                console.log(err);
                console.log("Dynamo Error. Not updating purchase info.");
                callback("DYNAMO PURCHASE ERROR");
              } else {
                // console.log(data);
                // console.log("Purchase update successful.");
                callback();
              }
            });
          },
          function(callback){
            if (rows.length > 1) {
              for (var i = 0; i < rows.length; i++) {
                docClient.update(recommendationInfo[i], function(err, data) {
                  if (err)  {
                    console.log(err);
                    console.log("Dynamo Error. Not updating recommendation info.");
                    callback("DYNAMO RECOMMENDATION ERROR");
                  } else {
                    // console.log(data);
                    // console.log("Recommendation"+i+" update successful.");
                  }
                });
              }
              //all recommendation updates successful
              callback();
            } else {
              //no need to update recommendations
              callback();
            }
            
          }
          ],
          //handle errors or update info
          function(err,results){
            // console.log("final callback");
            if(err == ("DYNAMO PURCHASE ERROR" || "DYNAMO RECOMMENDATION ERROR")) {
              res.json({message : "The input you provided is not valid"});
              return;
            } else if(err) {
              //respond failure
              res.json({message : "The input you provided is not valid"});
              return;
            } else {
              //respond success
              // console.log("Purchase successful");
              res.json({message: "The action was successful"});
              return;
            }
          }
          );
      } else if(rows.length == 0){
        //if no products match in database
        //console.log("no products with those asin found");
        res.json({message: "There are no products that match that criteria"});
        return;
      }
    });
});
}

exports.productsPurchased = function productsPurchased(req,res,dynamodb) {

  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  if(!req.session.admin) {
    res.json({message : "You must be an admin to perform this action"});
    return;
  }

  var docClient = new AWS.DynamoDB.DocumentClient(dynamodb);

  var userInfo = {
    TableName:"users",
    Key:{
        "username": req.body.username
    },
    AttributesToGet: [
      "userID"
    ]
  };

  // console.log("Attempting to login...");
  docClient.get(userInfo, function(err, data) {
    if(err) {
      //respond failure
      console.error("Unable to get userID. Error JSON:", JSON.stringify(err, null, 2));  
      res.json({message: "There are no users that match that criteria"});
      return;
    } else if(helper.isEmptyJSON(data)) {
      console.log("Unable to get userID.");
      res.json({message: "There are no users that match that criteria"});
      return;
    } else {
      
      var purchaseInfo = {
        TableName:"purchaseHistory",
        Key:{
            "userID": data.Item.userID
        }
      };

      docClient.get(purchaseInfo, function(err, data) {
        if(err) {
          //respond failure
          console.error("Unable to get purchase history. Error JSON:", JSON.stringify(err, null, 2));  
          res.json({message: "There are no users that match that criteria"});
          return;
        } else if(helper.isEmptyJSON(data)) {
          // console.log("Unable to login. No username/password match.");
          res.json({message: "There are no users that match that criteria"});
          return;
        } else {
          //got purchase history, package and reply
          var productsJSON = [];
          for (var key in data.Item) {
            if(data.Item.hasOwnProperty(key)) {
              // console.log("key: " + key);
              // console.log(key + " -> " + req.body[key]);
              if(key != "userID"){
                productsJSON.push({"productName":key,"quantity":data.Item[key]});
              }
            }
          }
          //console.log("productsJSON: " + JSON.stringify(productsJSON));
          //console.log("data: " + JSON.stringify(data));
          res.json({
            message: "The action was successful",
            products: productsJSON
          });
        }
      });
    }
  });
}

exports.getRecommendations = function getRecommendations(req,res,dynamodb) {

  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  var docClient = new AWS.DynamoDB.DocumentClient(dynamodb);

  var recommendationInfo = {
    TableName:"recommendations",
    Key:{
        "asin": req.body.asin
    }
  };

  docClient.get(recommendationInfo, function(err, data) {
    if(err) {
      //respond failure
      console.error("Unable to get recommendations. Error JSON:", JSON.stringify(err, null, 2));  
      res.json({message: "There are no recommendations for that product"});
      return;
    } else if(helper.isEmptyJSON(data)) {
      console.log("Unable to get recommendations. No data match in table.");
      res.json({message: "There are no recommendations for that product"});
      return;
    } else {
      //got recommendations, package and reply with top 5
      var productsJSON = [];
      for (var key in data.Item) {
        if(data.Item.hasOwnProperty(key)) {
          // console.log("key: " + key);
          // console.log(key + " -> " + req.body[key]);
          if(key != "asin"){
            productsJSON.push({"asin":key,"quantity":data.Item[key]});
          }
        }
      }
      console.log("productsJSON: " + JSON.stringify(productsJSON));

      //sort by quantity greatest to least
      productsJSON.sort(function (a, b) {
        return b.quantity - a.quantity;
      });

      console.log("sorted productsJSON: " + JSON.stringify(productsJSON));
      console.log("productsJSON.length: " + productsJSON.length);

      var productAsins = [];
      for(var i = 0;i< Math.min(5,productsJSON.length);i++) {
        productAsins.push({"asin":productsJSON[i].asin});
      }

      console.log("productAsins: " + JSON.stringify(productAsins));

      res.json({
        message: "The action was successful",
        products: productAsins
      });
    }
  });

}
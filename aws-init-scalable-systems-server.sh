#!/bin/bash 

#clear instance
rm -rf /home/ec2-user/scalable-systems/

#git clone to update
git clone https://rgibbs@bitbucket.org/rgibbs/scalable-systems.git /home/ec2-user/scalable-systems

#Startup script for AWS instances to run aws_server.js
node /home/ec2-user/scalable-systems/hw5/aws_server.js
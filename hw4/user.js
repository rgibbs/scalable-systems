var helper = require('./helper');
var mysql = require('mysql');
var async = require('async');


exports.registerUser  = function registerUser(req, res, pool) {
  // console.log("Got a POST request for the registration page");
  //console.log(pool);

  var registerAdmin = 0;

  //check if all fields valid, FAIL FAST!
  if(helper.isEmpty(req.body.fname)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmpty(req.body.lname)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmpty(req.body.address)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmpty(req.body.city)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmpty(req.body.state)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmpty(req.body.zip)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmpty(req.body.email)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmpty(req.body.username)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (helper.isEmpty(req.body.password)) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  async.parallel([
    function(callback) {
      //check if username available
      var sql = 'SELECT * FROM ?? WHERE ?? = ?';
      var inserts = ['users','username',req.body.username];
      sql = mysql.format(sql,inserts);

      pool.getConnection(function(err, connection) {
        //console.log(connection);
        connection.query(sql,function checkUsername(err,rows,fields) {
          connection.release();
          // console.log(rows);
          if(err) callback(err);

          // console.log("getUserByUsernamename");
          //if username is in database
          if(rows.length != 0) {
            callback("USERNAME EXISTS ALREADY");
            return;
          }

          //console.log("username not found");
          //if no username in database
          callback(null,rows);
        });
      });
    }],
    function(err,results){
      //console.log("final callback");
      if(err) {
        //respond failure
        res.json({message : "The input you provided is not valid"});
        return;
      } 
      
      // else everything checks out, add to users and return
      var sql = 'INSERT INTO ?? VALUES(?,?,?,?,?,?,?,?,?,?,?)';
      var inserts = ['users',req.body.username,req.body.password,
      registerAdmin,req.body.fname,req.body.lname,
      req.body.address,req.body.city,req.body.state,
      req.body.zip,req.body.email,0];
      sql = mysql.format(sql,inserts);

      //add to DB
      pool.getConnection(function(err, connection) {
        connection.query(sql, function addUsername(err,rows) {
          connection.release();
          if(err) throw err;
        });
      });

      //respond success
      res.json({message : "The action was successful"});
    }
  );
};


exports.login = function login(req, res, pool) {
  // console.log("Got a POST request for the login page");
  // console.log("Username is: " + req.body.username);
  // console.log("Password is: " + req.body.password);
  req.session.loggedIn = false;

  async.parallel([
    function(callback){
      getUserByUsername(req,pool,callback);
    }],
    function(err,results){
      // console.log("final callback");

      if(err) {
        //respond failure
        res.json({message : "There seems to be an issue with the username/password combination that you entered"});
        return;
      }
      // console.log("checking password...");
      // console.log("results: " + results);
      // console.log("results[0].admin: " + results[0].admin);
      // console.log("results[0].password: " + results[0].password);

      if(results[0].password != req.body.password) {
        res.json({message : "There seems to be an issue with the username/password combination that you entered"});
        return;
      }

      //initiate new session
      // TODO might need a session regenerate here?
      req.session.loggedIn = true;
      req.session.userId = results[0].userId;
      req.session.admin = results[0].admin;
      

      // console.log("about to create session");
      //update session
      //createSession(req,results[0].userId,results[0].admin);
      //respond success
      res.json({message : "Welcome " + results[0].fname});
    }
  );
}

exports.logout = function logout(req, res, pool) {
  // console.log("Got a POST request for the logout page");
  if(req.session.loggedIn) {
    req.session.loggedIn = false;
    res.json({message : "You have been successfully logged out"});
  } else {
    res.json({message : "You are not currently logged in"});
  }
}

exports.updateInfo = function updateInfo(req,res,pool) {

  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  //check which keys are present
  var sql = 'UPDATE ?? SET ';
  var sqlInsert = ['users'];

  var counter = 0;
  var usernameChangeRequested = false;
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!helper.isEmpty(req.body[key])){
        counter++;
        //console.log("non-empty:" + key + " -> " + req.body[key]);
        sql += '?? = ?, ';
        sqlInsert.push(key);
        sqlInsert.push(req.body[key]);
      }
    }
  }

  //if no values to update
  if(counter == 0) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  //toggle if requesting a username change
  if(!helper.isEmpty(req.body.username)) {
    usernameChangeRequested = true;
  }

  //trim the trailing commas
  sql = sql.substring(0,sql.length-2) + ' WHERE ?? = ?';

  // console.log("starting async...");

  //parallel check if username is valid and logged in correctly
  async.parallel([
    function(callback){
      //if username is changing
      if(usernameChangeRequested) {
        checkUsernameFree(req,pool,callback);
      }
      else {callback()};
    }
    ],
    //handle errors or update info
    function(err,results){
      // console.log("final callback");
      if(err) {
        //respond failure
        res.json({message : "The input you provided is not valid"});
        return;
      }

      // console.log("results.length: " + results.length);
      // console.log("results[0]: " + results[0]);
      // console.log("results[1].userId: " + results[1].userId);
      // console.log("results[1].lname: " + results[1].lname);
      // console.log("results[1].fname: " + results[1].fname);
      // console.log("results[1]:" + results[1]);
      // console.log("results[1][0]: " + results[1][0]);
      // console.log("results[1][1]: " + results[1][1]);
      // console.log("results[1][2]: " + results[1][2]);

      //update info
      sqlInsert.push("userId");
      sqlInsert.push(req.session.userId);
      sql = mysql.format(sql,sqlInsert);

      // console.log("sql: " + sql);
      // console.log("sqlInsert: " + sqlInsert);

      //update user info in DB
      pool.getConnection(function(err, connection) {
        connection.query(sql, function updateUserInfo(err,rows) {
          connection.release();
          if(err) {
            throw (err);
            res.json({message : "The input you provided is not valid"});
            return;
          }
          //respond success
          res.json({message: "The action was successful"});
        });
      });
    }
  );
}

function getUserByUsername(req,pool,callback) {
  // console.log("getting User...");
  var sql = 'SELECT * FROM ?? WHERE ?? = ?';
  var inserts = ['users','username',req.body.username];
  sql = mysql.format(sql,inserts);

  pool.getConnection(function(err, connection) {
    connection.query(sql,function checkUsername(err,rows,fields) {
      connection.release();
      if(err) {callback(err);return;}

      // console.log("getUserByUsernamename");
      //if username is in database
      if(rows.length == 1) {
        //console.log("found a row");
        //console.log("UserId: " + rows[0].userId);
        callback(null,rows[0]);
        return;
      } else if(rows.length == 0){
        //if no username in database
        callback("INVALID USERNAME");
        return;
      }
      callback("DB ERROR");
    });
  });
}

exports.viewUsers = function viewUsers(req,res,pool) {

  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  if(req.session.admin != 1) {
    res.json({message : "You must be an admin to perform this action"});
    return;
  }

  async.parallel([
    function(callback){
      getUsersByParams(req,pool,callback);
    }],
    //handle errors or update info
    function(err,results){
      // console.log("final callback");
      if(err == "NO MATCHING USERS FOUND") {
        res.json({message : "There are no users that match that criteria"});
        return;
      } else if(err) {
        //respond failure
        throw err;
        res.json({message : "There are no users that match that criteria"});
        return;
      }
      // console.log("results: " + results);
      // console.log("results.length: " + results.length);
      // console.log("results[0]: " + results[0]);
      // console.log("results[1]: " + results[1]);
      // console.log("results[1].length: " + results[1].length);
      // console.log("results[1].userId: " + results[1].userId);
      // console.log("results[1].admin: " + results[1].admin);
      // console.log("results[1].fname: " + results[1].fname);
      // console.log("results[1].lname: " + results[1].lname);
      // console.log("results[1].password: " + results[1].password);

      // console.log("results.length: " + results.length);
      // console.log("results[0]: " + results[0]);
      // console.log("results[1].userId: " + results[1].userId);

      // console.log("sql: " + sql);
      // console.log("sqlInsert: " + sqlInsert);

      //respond success
      res.json({message: "The action was successful",user:results[0]});
    }
  );
}

function checkUsernameFree(req,pool,callback) {
  // console.log("checking username...");
  var sql = 'SELECT * FROM ?? WHERE ?? = ?';
  var inserts = ['users','username',req.body.username];
  sql = mysql.format(sql,inserts);
  pool.getConnection(function(err, connection) {
    connection.query(sql,function checkUsername(err,rows,fields) {
      connection.release();
      if(err) {callback(err);return;}

      if(rows.length == 1) {
        // console.log("found a row")
        callback("INVALID USERNAME");
        return;
      } else if(rows.length == 0){
        //if no username in database
        callback();
        return;
      }
      callback("DB ERROR");
    });
  });
}


function getUsersByParams(req,pool,callback) {

  var counter = 0;
  sql = 'SELECT ??,??,?? FROM ?? WHERE';
  sqlInsert = ['fname','lname','userId','users'];
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!helper.isEmpty(req.body[key])){
        counter++;
        // console.log("non-empty:" + key + " -> " + req.body[key]);
        sql += ' ?? LIKE ? AND';
        sqlInsert.push(key);
        sqlInsert.push('%' + req.body[key] + '%');
      }
    }
  }

  //if no params to filter by
  if (counter == 0) {
    //trim the 'where'
    sql = sql.substring(0,sql.length-5);
  } else {
    //trim the 'and'
    sql = sql.substring(0,sql.length-3);
  }


  sql = mysql.format(sql,sqlInsert);

  // console.log("sql: " + sql);
  pool.getConnection(function(err, connection) {
    connection.query(sql,function getSqlUsers(err,rows,fields) {
      connection.release();
      if(err) {callback(err);return;}

      if(rows.length == 0) {
        callback("NO MATCHING USERS FOUND");
        return;
      }
      // console.log("found some rows! returning them...");
      callback(null,rows);
    }
    );
  });

}
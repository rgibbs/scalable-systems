var helper = require('./helper');
var mysql = require('mysql');
var async = require('async');


exports.addProducts = function addProducts(req,res,pool) {

  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  if(req.session.admin != 1) {
    res.json({message : "You must be an admin to perform this action"});
    return;
  }

  var counter = 0;
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!helper.isEmpty(req.body[key])){
        counter++;
      }
    }
  }

  //if not all parameters present
  if(counter != 4) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  //check if ASIN is valid before adding
  async.parallel([
    function(callback){
      checkAsinFree(req,pool,callback);
    }],
    //handle errors or update info
    function(err,results){
      // console.log("final callback");
      if(err == "INVALID ASIN") {
        res.json({message : "The input you provided is not valid"});
        return;
      } else if(err) {
        //respond failure
        res.json({message : "The input you provided is not valid"});
        return;
      }

      // console.log("results.length: " + results.length);
      // console.log("results[0]: " + results[0]);
      // console.log("results[1].userId: " + results[1].userId);

      // console.log("sql: " + sql);
      // console.log("sqlInsert: " + sqlInsert);

      // else everything checks out, add to users and return
      var sql = 'INSERT INTO ??  (??,??,??,??) VALUES(?,?,?,?)';
      var inserts = ['products', 'asin', 'productName', 'productDescription',
                     'category', req.body.asin, req.body.productName,
                     req.body.productDescription, req.body.group];
      sql = mysql.format(sql,inserts);

      //add to DB
      pool.getConnection(function(err, connection) {
        connection.query(sql, helper.throwError);
        connection.release();
      });

      //respond success
      res.json({message: "The action was successful"});
    }
  );
}


exports.modifyProduct = function modifyProduct (req,res,pool) {

  if(!req.session.loggedIn) {
    res.json({message : "You are not currently logged in"});
    return;
  }

  if(req.session.admin != 1) {
    res.json({message : "You must be an admin to perform this action"});
    return;
  }

  //check which keys are present
  var counter = 0;
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!helper.isEmpty(req.body[key])){
        counter++;
      }
    }
  }

  //if no values to update
  if(counter != 4) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  //update info
  var sql = 'UPDATE ?? SET ?? = ?, ?? = ?, ?? = ? WHERE ?? = ?';
  var sqlInsert = ['products', 'productName', req.body.productName,
                   'productDescription', req.body.productDescription,
                   'category', req.body.group,
                   'asin', req.body.asin];
  
  
  sql = mysql.format(sql,sqlInsert);
  // console.log("sql: " + sql);

  pool.getConnection(function(err, connection) {
    connection.query(sql,function updateSqlInfo(err,rows,fields) {
      connection.release();
      if(err) {
        throw (err);
        res.json({message : "The input you provided is not valid"});
        return;
      }
      //respond success
      res.json({message: "The action was successful"});
    });      
  });
}

exports.viewProducts = function viewProducts(req,res,pool) {

  var counter = 0;
  sql = 'SELECT ??,?? FROM ?? WHERE';
  sqlInsert = ['asin','productName','products'];
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!helper.isEmpty(req.body[key])){
        counter++;
        if(key == 'asin') {
          // console.log("non-empty:" + key + " -> " + req.body[key]);
          sql += ' ?? = ? AND';
          sqlInsert.push(key);
          sqlInsert.push(req.body[key]);
        } else if (key == 'keyword') {
          sql += ' (MATCH(??) AGAINST(?) OR MATCH(??) AGAINST(?)) AND';
          sqlInsert.push('productName');
          sqlInsert.push(req.body[key]);
          sqlInsert.push('productDescription');
          sqlInsert.push(req.body[key]);
        } else if (key == 'group') {
          sql += ' MATCH(??) AGAINST(?) AND';
          sqlInsert.push('category');
          sqlInsert.push(req.body[key]);
        }
      }
    }
  }

  //if no params to filter by
  if (counter == 0) {
    //trim the 'where'
    sql = sql.substring(0,sql.length-5);
  } else {
    //trim the 'and'
    sql = sql.substring(0,sql.length-3);
  }


  sql = mysql.format(sql,sqlInsert);

  // console.log("sql: " + sql);
  pool.getConnection(function(err, connection) {
    connection.query(sql,function getSqlProducts(err,rows,fields) {
      connection.release();
      if(err) {
        throw(err);
        res.json({message:"There are no products that match that criteria"});
        return;
      }

      if(rows.length == 0) {
        res.json({message:"There are no products that match that criteria"});
        return;
      }
      
      //respond success
      res.json({message: "The action was successful",product:rows});
    });
  });
}

function checkAsinFree(req,pool,callback) {
  // console.log("checking username...");
  var sql = 'SELECT * FROM ?? WHERE ?? = ?';
  var inserts = ['products','asin',req.body.asin];
  sql = mysql.format(sql,inserts);
  pool.getConnection(function(err, connection) {
    connection.query(sql,function checkUsername(err,rows,fields) {
      connection.release();
      if(err) {callback(err);return;}

      // console.log("getUserByUsernamename");
      //if username is in database
      if(rows.length == 1) {
        // console.log("found a row")
        callback("INVALID ASIN");
        return;
      } else if(rows.length == 0){
        //if no username in database
        callback();
        return;
      }
      callback("DB ERROR");
    });
});
}

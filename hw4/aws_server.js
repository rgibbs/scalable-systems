var express = require('express');
var router = express.Router();
var session = require('express-session');
var request = require('request');
var mysql = require('mysql');
var mysqlStore = require('express-mysql-session');
var async = require('async');
var bodyParser = require('body-parser');
var uid = require('uid-safe');
var ON_DEATH = require('death');


var user = require('./user');
var product = require('./product');
var helper = require('./helper');

var pool  = mysql.createPool({
  connectionLimit : 3,
  host: 'scalablesystemsmysql.cb9anwh1e9es.us-east-1.rds.amazonaws.com',
  port: '4545',
  user: 'rgibbs',
  password: 'abth4545R!',
  database: 'hw2'
});


var storeOptions = {
  checkExpirationInterval: 60000, //how often to clear out expired sessions in ms
  expiration: 900000,             //max time before session expires in ms
  createDatabaseTable: true,      //create table if it doesn't exist
  schema: {
    tableName: 'sessions',
    columnNames: {
      session_id: 'session_id',
      expires: 'expires',
      data: 'data'
    }
  }

}

var sessionStore = new mysqlStore(storeOptions,pool);


pool.getConnection(function(err, connection) {

  clearSql = 'TRUNCATE TABLE ??';
  clearInserts = ['users'];
  clearSql = mysql.format(clearSql,clearInserts);
  connection.query(clearSql, helper.throwError);

  clearSql = 'TRUNCATE TABLE ??';
  clearInserts = ['sessions'];
  clearSql = mysql.format(clearSql,clearInserts);
  connection.query(clearSql, helper.throwError);

  //ADD DEFAULT USERS
  var defaultUsersSql = 'INSERT INTO ?? (??,??,??,??,??) VALUES(?,?,?,?,?)'
  var defaultInserts = ['users','username','password','fname','lname', 'admin',
  'jadmin','admin','Jenny','Admin', '1'];
  defaultUsersSql = mysql.format(defaultUsersSql,defaultInserts);
  connection.query(defaultUsersSql, helper.throwError);

  connection.release();

});

var app = express();
app.use(bodyParser.json());
app.use(session({
  saveUnitialized: false,
  key: 'session_cookie_name',
  secret: 'darkLightning',
  resave: false,
  saveUnitialized: false,
  cookie: {maxAge: 900000},
  store: sessionStore
}))

//called when process is killed (SIGINT,SIGTERM,SIGQUIT)
ON_DEATH(function(signal, err) {
  
  console.log("\nBOOM Headshot...");
  
  //cleanup code
  pool.end();

  console.log("Cleanup complete. Process dying NOW!!!")
  process.exit();

})


/////////////////////
// SUPPORTED PAGES //
/////////////////////

router.post('/registerUser', registerUserRequest);
router.post('/login', loginRequest);
router.post('/logout', logoutRequest);
router.post('/updateInfo', updateInfoRequest);
router.post('/viewUsers', viewUsersRequest);
router.post('/addProducts', addProductsRequest);
router.post('/modifyProduct', modifyProductRequest);
router.post('/viewProducts', viewProductsRequest);
router.get('/health', helper.healthy);


app.use('/',router);


var server = app.listen(3000, '0.0.0.0', function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Server app listening at http://%s:%s", host, port)

});

function registerUserRequest(req, res) {user.registerUser(req,res,pool)};
function loginRequest(req, res) {user.login(req,res,pool)};
function logoutRequest(req, res) {user.logout(req,res,pool)};
function updateInfoRequest(req,res) {user.updateInfo(req,res,pool)};
function viewUsersRequest(req,res) {user.viewUsers(req,res,pool)};
function addProductsRequest(req,res) {product.addProducts(req,res,pool)};
function modifyProductRequest(req,res) {product.modifyProduct(req,res,pool)};
function viewProductsRequest(req,res) {product.viewProducts(req,res,pool)};



//////////////////////
// HELPER FUNCTIONS //
//////////////////////

exports.throwError = function(err) {
  if (err) throw err;
};

exports.isEmpty = function(value) {
  return (typeof value == 'string' && !value.trim() ||
    typeof value == 'undefined' || 
    value === null ||
    value == null);
};

exports.healthy = function(req,res) {
  res.json({health: "alive"});
}




//checks results of state pair query
// function querySqlForStateCode(req, callback) {
//   //get stateCode/state pair
//   var stateSql = 'SELECT * FROM ?? WHERE (?? = ? OR ?? = ?)';
//   var stateInserts = ['stateCodes','stateCode',req.body.state,'state',req.body.state];
//   stateSql = mysql.format(stateSql,stateInserts);
//   connection.query(stateSql,function getStatePair (err,rows,fields) { 
//     if (err) callback(err);
//     console.log("getStatePair");
//     //if state not in database
//     if(rows.length == 0) {
//       callback("INVALID STATE");
//       return;
//     }
//     //if state in database, no error and return rows
//     callback(null, rows);
//   });
// }



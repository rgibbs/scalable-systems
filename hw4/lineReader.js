/*
lineReader will extract the records from amazon-meta.txt one at a time as
file is too large to read all at once.  In order to add records to a database you need to add code below to insert records

This code depnds on "line-reader"

You need to install line-reader by using the following command:
npm install line-reader

*/

//This assumes that you're using mysql.  You'll need to change this if you're using another database
var mysql      = require('mysql'),
    co         = require('co'),
    wrapper    = require('co-mysql');
var query;
var jsonRecord;
var execute = true;
var query = "";
var totalRecords = 0;
var fs = require('fs');
var args = process.argv.slice(2);
var fileName = args[0];
console.log("Processing lines for:")
console.log(fileName);
//var fileName = "productRecords.json";


var lineReader = require('line-reader');


//You need to change this to be appropriate for your system
var connection = mysql.createConnection({
  host: 'scalablesystemsmysql.cb9anwh1e9es.us-east-1.rds.amazonaws.com',
  port: '4545',
  user: 'rgibbs',
  password: 'abth4545R!',
  database: 'hw2'
});

connection.connect();
var sql = wrapper(connection);

var values = ""; //The records read from the file.
var numRecords = 0; //The current number of records read from the file.
var recordBlock = 1000; //The number of records to write at once.
var test;
var maxDescriptionLength = 0;
var maxTitleLength = 0;
lineReader.eachLine(fileName, function(line, last) {
  execute = false;
  currentLine = line.toString();
  try{
    //test = "Haven't touched values yet."
    jsonRecord = JSON.parse(currentLine);
    //console.log(jsonRecord);
    //console.log(currentLine);


    // if (jsonRecord.description != undefined) {
    //   if (jsonRecord.description.length > maxDescriptionLength) {
    //     maxDescriptionLength = jsonRecord.description.length;
    //   }
    // }

    // if (jsonRecord.title != undefined) {
    //   if (jsonRecord.title.length > maxTitleLength) {
    //     maxTitleLength = jsonRecord.title.length;numRecords
    //   }
    // }

    // if(numRecords % recordBlock == 0) {
    //   console.log("numRecords: " + numRecords);
    // }

    // if(last) {
    //   // console.log("maxDescriptionLength:" + maxDescriptionLength);
    //   // console.log("maxTitleLength: " + maxTitleLength);
    //   // process.exit();
    // }

    if (numRecords) {
      values += "\n";
    }

    values += `('${jsonRecord.title}', '${jsonRecord.categories}', '${jsonRecord.description}', '${jsonRecord.asin}'),`;
    // //test = "Values altered!"
    numRecords++;

    //Change the query to align with your schema
    if (numRecords == recordBlock || last) {
      values = values.slice(0,-1);
      query = `INSERT INTO products (productName, category, productDescription, asin) VALUES ${values};`; //Template, replaces ${values} with the value of values.
      values = "";
      numRecords = 0;
      execute = true;
      //console.log(query);
    }
  }catch(err) {
    execute = false;//there was a quote in the text and the parse failed ... skip insert
    //console.log(err);
    //console.log(test);
  }

  if(execute){//***************Need to put logic to insert into your db*****************************************
    co(function* () {
        var resp = yield sql.query(query);
        //console.log(mysql.format(query))
        //connection.query(mysql.format(query));

        totalRecords += recordBlock;
        console.log(totalRecords + " records inserted.");

        if(last) {
          process.exit();
        }
    });
  }//if(execute)
});//lineReader.eachLine

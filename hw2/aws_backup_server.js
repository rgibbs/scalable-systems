var express = require('express');
var request = require('request');
var mysql = require('mysql');
var async = require('async');
var bodyParser = require('body-parser');

var connection = mysql.createConnection({
  host: 'scalablesystemsmysql.cb9anwh1e9es.us-east-1.rds.amazonaws.com',
  port: '4545',
  user: 'rgibbs',
  password: 'abth4545R!',
  database: 'hw2'
});

// var primaryServerUrl = 'http://127.0.0.1:3000/health';

connection.connect();

// var clearSql = 'TRUNCATE TABLE ??';
// var clearInserts = ['activeSessions'];
// clearSql = mysql.format(clearSql,clearInserts);
// connection.query(clearSql, printSqlError);

// clearSql = 'TRUNCATE TABLE ??';
// clearInserts = ['products'];
// clearSql = mysql.format(clearSql,clearInserts);
// connection.query(clearSql, printSqlError);

// clearSql = 'TRUNCATE TABLE ??';
// clearInserts = ['users'];
// clearSql = mysql.format(clearSql,clearInserts);
// connection.query(clearSql, printSqlError);

// //ADD DEFAULT USERS
// var defaultUsersSql = 'INSERT INTO ?? (??,??,??,??,??) VALUES(?,?,?,?,?)'
// var defaultInserts = ['users','username','password','fname','lname', 'admin',
// 'jadmin','admin','Jenny','Admin', '1'];
// defaultUsersSql = mysql.format(defaultUsersSql,defaultInserts);
// connection.query(defaultUsersSql, printSqlError);

var app = express();
app.use(bodyParser.json());

// request(primaryServerUrl, {timeout: 10000},function (error, response, body) {
//   if (!error && response.statusCode == 200) {
//     console.log(body)
//   } else {
//     console.log("Primary server is dead");
//   }
// });


/////////////////////
// SUPPORTED PAGES //
/////////////////////
/////////////////////
// SUPPORTED PAGES //
/////////////////////

app.post('/registerUser', handleRegisterUserRequest);
app.post('/login', handleLoginRequest);
app.post('/logout', handleLogoutRequest);
app.post('/updateInfo', handleUpdateInfoRequest);
app.post('/viewUsers', handleViewUsersRequest);
app.post('/addProducts', handleAddProductsRequest);
app.post('/modifyProduct', handleModifyProductRequest);
app.post('/viewProducts', handleViewProductsRequest);
app.get('/health', handleHealthRequest);


var server = app.listen(3000, '0.0.0.0', function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Server app listening at http://%s:%s", host, port)

});

function handleHealthRequest(req,res) {
  res.json({health: "alive"});
}

function handleRegisterUserRequest (req, res) {
  // console.log("Got a POST request for the registration page");

  var registerAdmin = 0;

  //check if all fields valid, FAIL FAST!
  if(isEmpty(req.body.fname)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (isEmpty(req.body.lname)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (isEmpty(req.body.address)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (isEmpty(req.body.city)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (isEmpty(req.body.state)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (isEmpty(req.body.zip)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (isEmpty(req.body.email)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (isEmpty(req.body.username)) {
    res.json({message : "The input you provided is not valid"});
    return;
  } else if (isEmpty(req.body.password)) {
    res.json({message : "The input you provided is not valid"});
    return;
  }


  async.parallel([
    function(callback) {
      //check if username available
      var sql = 'SELECT * FROM ?? WHERE ?? = ?';
      var inserts = ['users','username',req.body.username];
      sql = mysql.format(sql,inserts);
      connection.query(sql,function checkUsername(err,rows,fields) {
        if(err) callback(err);

        // console.log("getUserByUsernamename");
        //if username is in database
        if(rows.length != 0) {
          callback("USERNAME EXISTS ALREADY");
          return;
        }

        //console.log("username not found");
        //if no username in database
        callback(null,rows);
      });
    }],
    function(err,results){
      //console.log("final callback");
      if(err) {
        //respond failure
        res.json({message : "The input you provided is not valid"});
        return;
      } 
      

      // else everything checks out, add to users and return
      var sql = 'INSERT INTO ?? VALUES(?,?,?,?,?,?,?,?,?,?,?)';
      var inserts = ['users',req.body.username,req.body.password,
                          registerAdmin,req.body.fname,req.body.lname,
                          req.body.address,req.body.city,req.body.state,
                          req.body.zip,req.body.email,0];
      sql = mysql.format(sql,inserts);

      //add to DB
      connection.query(sql, printSqlError);

      //respond success
      res.json({message : "The action was successful"});

    }
  );
}

function handleLoginRequest(req, res) {
  // console.log("Got a POST request for the login page");

  // console.log("Username is: " + req.body.username);
  // console.log("Password is: " + req.body.password);

  async.parallel([
    function(callback){
      getUserByUsername(req,callback);
    }
    ],
    function(err,results){
      // console.log("final callback");
      if(err) {
        //respond failure
        res.json({message : "There seems to be an issue with the username/password combination that you entered"});
        return;
      }
      // console.log("checking password...");
      // console.log("results: " + results);
      // console.log("results[0]: " + results[0]);
      // console.log("results[0].password: " + results[0].password);

      if(results[0].password != req.body.password) {
        res.json({message : "There seems to be an issue with the username/password combination that you entered"});
        return;
      }
      // console.log("about to create session");
      //update session
      createSession(req,results[0].userId,results[0].admin);
      //respond success
      res.json({message : "Welcome "+results[0].fname});
    }
  );
}

function handleLogoutRequest (req, res) {
  // console.log("Got a POST request for the logout page");


  var sessionCheckSql = 'SELECT * FROM ?? WHERE ?? = ?';
  var insertCheck = ['activeSessions', 'client_ip', req.ip];
  sessionCheckSql = mysql.format(sessionCheckSql,insertCheck);

  connection.query(sessionCheckSql,function(err,rows,fields){
    //if no entry in the table that matches, respond not logged in  
    if(rows.length == 0) {
      res.json({message : "You are not currently logged in"});
    }
    //if entry is already in table, logout
    else {
      var sessionDeleteSql = 'DELETE FROM ?? WHERE ?? = ?';
      var insertDelete = ['activeSessions', 'client_ip', req.ip];
      sessionDeleteSql = mysql.format(sessionDeleteSql,insertDelete);
      // console.log("sessionDeleteSql is: " + sessionDeleteSql);

      connection.query(sessionDeleteSql, printSqlError);
      res.json({message : "You have been successfully logged out"});
    }
  });
}

function handleUpdateInfoRequest (req,res) {

  //check which keys are present
  var sql = 'UPDATE ?? SET ';
  var sqlInsert = ['users'];

  var counter = 0;
  var usernameChangeRequested = false;
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!isEmpty(req.body[key])){
        counter++;
        //console.log("non-empty:" + key + " -> " + req.body[key]);
        sql += '?? = ?, ';
        sqlInsert.push(key);
        sqlInsert.push(req.body[key]);
      }
    }
  }

  //if no values to update
  if(counter == 0) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  //toggle if requesting a username change
  if(!isEmpty(req.body.username)) {
    usernameChangeRequested = true;
  }

  //trim the trailing commas
  sql = sql.substring(0,sql.length-2) + ' WHERE ?? = ?';

  // console.log("starting async...");

  //parallel check if username is valid and logged in correctly
  async.parallel([
    function(callback){
      //if username is changing
      if(usernameChangeRequested) {
        checkUsernameFree(req,callback);
      }
      else {callback()};
    },
    function(callback){
      getActiveSession(req,callback);
    }
    ],
    //handle errors or update info
    function(err,results){
      // console.log("final callback");
      if(err == "INVALID USERNAME") {
        res.json({message : "The input you provided is not valid"});
        return;
      } else if (err == "NOT LOGGED IN") {
        res.json({message : "You are not currently logged in"});
        return;
      } else if(err) {
        //respond failure
        res.json({message : "The input you provided is not valid"});
        return;
      }

      // console.log("results.length: " + results.length);
      // console.log("results[0]: " + results[0]);
      // console.log("results[1].userId: " + results[1].userId);
      // console.log("results[1].lname: " + results[1].lname);
      // console.log("results[1].fname: " + results[1].fname);
      // console.log("results[1]:" + results[1]);
      // console.log("results[1][0]: " + results[1][0]);
      // console.log("results[1][1]: " + results[1][1]);
      // console.log("results[1][2]: " + results[1][2]);

      
      //update info
      sqlInsert.push("userId");
      sqlInsert.push(results[1].userId);
      sql = mysql.format(sql,sqlInsert);

      // console.log("sql: " + sql);
      // console.log("sqlInsert: " + sqlInsert);
      
      //update session
      updateSession(req,results[1].userId,results[1].admin);

      connection.query(sql,function updateSqlInfo(err,rows,fields) {
        if(err) {throw (err);return;}
        //respond success
        res.json({message: "The action was successful"});
      });
    }
  );
}


function handleViewUsersRequest(req,res) {

  //parallel check if username is valid and logged in correctly
  async.parallel([
    function(callback){
      getUsersByParams(req,callback);
    },
    function(callback){
      getActiveSession(req,callback);
    }
    ],
    //handle errors or update info
    function(err,results){
      // console.log("final callback");
      if(err == "NO MATCHING USERS FOUND") {
        res.json({message : "There are no users that match that criteria"});
        return;
      } else if (err == "NOT LOGGED IN") {
        res.json({message : "You are not currently logged in"});
        return;
      } else if(err) {
        //respond failure
        res.json({message : "There are no users that match that criteria"});
        return;
      }
      // console.log("results: " + results);
      // console.log("results.length: " + results.length);
      // console.log("results[0]: " + results[0]);
      // console.log("results[1]: " + results[1]);
      // console.log("results[1].length: " + results[1].length);
      // console.log("results[1].userId: " + results[1].userId);
      // console.log("results[1].admin: " + results[1].admin);
      // console.log("results[1].fname: " + results[1].fname);
      // console.log("results[1].lname: " + results[1].lname);
      // console.log("results[1].password: " + results[1].password);

      //make sure user is an admin
      if(!results[1].admin) {
        res.json({message : "You must be an admin to perform this action"});
        return;
      }

      // console.log("results.length: " + results.length);
      // console.log("results[0]: " + results[0]);
      // console.log("results[1].userId: " + results[1].userId);

      // console.log("sql: " + sql);
      // console.log("sqlInsert: " + sqlInsert);
      
      //update session
      updateSession(req,results[1].userId,results[1].admin);


      //respond success
      res.json([{message: "The action was successful"},
                {user:results[0]}]);
    }
  );
}

function handleAddProductsRequest(req,res) {

  var counter = 0;
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!isEmpty(req.body[key])){
        counter++;
      }
    }
  }

  //if not all parameters present
  if(counter != 4) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  //parallel check if username is valid and logged in correctly
  async.parallel([
    function(callback){
      checkAsinFree(req,callback);
    },
    function(callback){
      getActiveSession(req,callback);
    }
    ],
    //handle errors or update info
    function(err,results){
      // console.log("final callback");
      if(err == "INVALID ASIN") {
        res.json({message : "The input you provided is not valid"});
        return;
      } else if (err == "NOT LOGGED IN") {
        res.json({message : "You are not currently logged in"});
        return;
      } else if(err) {
        //respond failure
        res.json({message : "The input you provided is not valid"});
        return;
      }

      //make sure user is an admin
      if(!results[1].admin) {
        res.json({message : "You must be an admin to perform this action"});
        return;
      }

      // console.log("results.length: " + results.length);
      // console.log("results[0]: " + results[0]);
      // console.log("results[1].userId: " + results[1].userId);

      // console.log("sql: " + sql);
      // console.log("sqlInsert: " + sqlInsert);

      // else everything checks out, add to users and return
      var sql = 'INSERT INTO ??  (??,??,??,??) VALUES(?,?,?,?)';
      var inserts = ['products', 'asin', 'productName', 'productDescription',
                     'category', req.body.asin, req.body.productName,
                     req.body.productDescription, req.body.group];
      sql = mysql.format(sql,inserts);

      //add to DB
      connection.query(sql, printSqlError);
      
      //update session
      updateSession(req,results[1].userId,results[1].admin);


      //respond success
      res.json({message: "The action was successful"});
    }
  );
}

function handleModifyProductRequest (req,res) {

  //check which keys are present
  var counter = 0;
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!isEmpty(req.body[key])){
        counter++;
      }
    }
  }

  //if no values to update
  if(counter != 4) {
    res.json({message : "The input you provided is not valid"});
    return;
  }

  //parallel check if username is valid and logged in correctly
  async.parallel([
    function(callback){
      getActiveSession(req,callback);
    }
    ],
    //handle errors or update info
    function(err,results){
      // console.log("final callback");
      if (err == "NOT LOGGED IN") {
        res.json({message : "You are not currently logged in"});
        return;
      } else if(err) {
        //respond failure
        res.json({message : "The input you provided is not valid"});
        return;
      }

      //make sure user is an admin
      if(!results[0].admin) {
        res.json({message : "You must be an admin to perform this action"});
        return;
      }

      //update session
      updateSession(req,results[0].userId,results[0].admin);

      //update info
      var sql = 'UPDATE ?? SET ?? = ?, ?? = ?, ?? = ? WHERE ?? = ?';
      var sqlInsert = ['products', 'productName', req.body.productName,
                       'productDescription', req.body.productDescription,
                       'category', req.body.group,
                       'asin', req.body.asin];
      
      
      sql = mysql.format(sql,sqlInsert);
      // console.log("sql: " + sql);
      
      connection.query(sql,function updateSqlInfo(err,rows,fields) {
        if(err) {throw (err);return;}
        //respond success
        res.json({message: "The action was successful"});
      });
    }
  );
}

function handleViewProductsRequest(req,res) {


  var counter = 0;
  sql = 'SELECT ??,?? FROM ?? WHERE';
  sqlInsert = ['asin','productName','products'];
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!isEmpty(req.body[key])){
        counter++;
        if(key == 'asin') {
          // console.log("non-empty:" + key + " -> " + req.body[key]);
          sql += ' ?? = ? AND';
          sqlInsert.push(key);
          sqlInsert.push(req.body[key]);
        } else if (key == 'keyword') {
          sql += ' (?? LIKE ? OR ?? LIKE ?) AND';
          sqlInsert.push('productName');
          sqlInsert.push('%'+req.body[key]+'%');
          sqlInsert.push('productDescription');
          sqlInsert.push('%'+req.body[key]+'%');
        } else if (key == 'group') {
          sql += ' ?? = ? AND';
          sqlInsert.push('category');
          sqlInsert.push(req.body[key]);
        }
      }
    }
  }

  //if no params to filter by
  if (counter == 0) {
    //trim the 'where'
    sql = sql.substring(0,sql.length-5);
  } else {
    //trim the 'and'
    sql = sql.substring(0,sql.length-3);
  }


  sql = mysql.format(sql,sqlInsert);

  // console.log("sql: " + sql);
  connection.query(sql,function getSqlProducts(err,rows,fields) {
    if(err) {
      throw(err);
      res.json({message:"There are no products that match that criteria"});
      return;
    }

    if(rows.length == 0) {
      res.json({message:"There are no products that match that criteria"});
      return;
    }
    
    //respond success
    res.json({product:rows});
    
  }
  );


}

//////////////////////
// HELPER FUNCTIONS //
//////////////////////

function printSqlError(err) {
  if (err) throw err;
}

function isEmpty(value) {
  return (typeof value == 'string' && !value.trim() ||
    typeof value == 'undefined' || 
    value === null ||
    value == null);
};

function getUserByUsername(req,callback) {
  // console.log("getting User...");
  var sql = 'SELECT * FROM ?? WHERE ?? = ?';
  var inserts = ['users','username',req.body.username];
  sql = mysql.format(sql,inserts);
  connection.query(sql,function checkUsername(err,rows,fields) {
    if(err) {callback(err);return;}

    // console.log("getUserByUsernamename");
    //if username is in database
    if(rows.length == 1) {
      //console.log("found a row");
      //console.log("UserId: " + rows[0].userId);
      callback(null,rows[0]);
      return;
    } else if(rows.length == 0){
      //if no username in database
      callback("INVALID USERNAME");
      return;
    }
    callback("DB ERROR");
  });
}

function checkUsernameFree(req,callback) {
  // console.log("checking username...");
  var sql = 'SELECT * FROM ?? WHERE ?? = ?';
  var inserts = ['users','username',req.body.username];
  sql = mysql.format(sql,inserts);
  connection.query(sql,function checkUsername(err,rows,fields) {
    if(err) {callback(err);return;}

    // console.log("getUserByUsernamename");
    //if username is in database
    if(rows.length == 1) {
      // console.log("found a row")
      callback("INVALID USERNAME");
      return;
    } else if(rows.length == 0){
      //if no username in database
      callback();
      return;
    }
    callback("DB ERROR");
  });
}

function checkAsinFree(req,callback) {
  // console.log("checking username...");
  var sql = 'SELECT * FROM ?? WHERE ?? = ?';
  var inserts = ['products','asin',req.body.asin];
  sql = mysql.format(sql,inserts);
  connection.query(sql,function checkUsername(err,rows,fields) {
    if(err) {callback(err);return;}

    // console.log("getUserByUsernamename");
    //if username is in database
    if(rows.length == 1) {
      // console.log("found a row")
      callback("INVALID ASIN");
      return;
    } else if(rows.length == 0){
      //if no username in database
      callback();
      return;
    }
    callback("DB ERROR");
  });
}


function createSession(req, userId, admin) {
  // console.log("creating session...");
  //at this point user has been validated
  var sessionCheckSql = 'SELECT * FROM ?? WHERE ?? = ?';
  var insertCheck = ['activeSessions', 'client_ip', req.ip];
  sessionCheckSql = mysql.format(sessionCheckSql,insertCheck);
  // console.log("sessionCheckSql is: " + req.ip)

  connection.query(sessionCheckSql,function(err,rows,fields){
    if(err) {callback(err);return;}
    //if no entry in the table that matches, 
    //this is a new session, simply add
    if(rows.length == 0) {
      var sessionAddSql = 'INSERT INTO ?? (??,??,??,??) VALUES(?,?,?,NOW())';
      var insertAdd = ['activeSessions', 'userId','client_ip','admin','timestamp',
                      userId, req.ip,admin];
      sessionAddSql = mysql.format(sessionAddSql,insertAdd);
      // console.log("sessionAddSql is: " + sessionAddSql);

      connection.query(sessionAddSql, printSqlError);
    }
    //if entry is already in table, update session
    else {
      var sessionUpdateSql = 'UPDATE ?? SET ?? = ?,  ?? = ?, ?? = NOW() WHERE ?? = ?';
      var insertUpdate = ['activeSessions', 'userId', userId, 'admin', admin,
                          'timestamp', 'client_ip', req.ip];
      sessionUpdateSql = mysql.format(sessionUpdateSql,insertUpdate);
      // console.log("sessionUpdateSql is: " + sessionUpdateSql);

      connection.query(sessionUpdateSql, printSqlError);
    }
  });
}

function updateSession(req, userId, admin) {
  var sessionUpdateSql = 'UPDATE ?? SET ?? = ?, ?? = ?, ?? = NOW() WHERE ?? = ?';
  var insertUpdate = ['activeSessions', 'userId', userId, 'admin', admin,
                      'timestamp', 'client_ip', req.ip];
  sessionUpdateSql = mysql.format(sessionUpdateSql,insertUpdate);
  // console.log("sessionUpdateSql is: " + sessionUpdateSql);

  connection.query(sessionUpdateSql, printSqlError);
}

//gets the active session user
//if timeout expired, deletes active session 
function getActiveSession(req,callback) {
  //check if this ip is currently logged in
  var sessionCheckSql = 'SELECT TIMESTAMPDIFF(minute,??,NOW()) AS ?? , ??, ?? FROM ?? WHERE ?? = ?';
  var insertCheck = ['timestamp', 'deltaTime', 'userId', 'admin' , 'activeSessions', 'client_ip', req.ip];
  sessionCheckSql = mysql.format(sessionCheckSql,insertCheck);
  //console.log("SessionChecksql is: " + sessionCheckSql);

  connection.query(sessionCheckSql,function(err,rows,fields){
    if(err) {callback(err);return;}

    //if no entry in the table that matches, 
    //respond not logged in  
    if(rows.length == 0) {
      callback("NOT LOGGED IN");
      return;
    }
    //if entry is already in table
    //check if time is still valid or need to trigger logout
    else if (rows[0].deltaTime >= 15) {
      console.log("Session timed out.");
      var sessionDeleteSql = 'DELETE FROM ?? WHERE ?? = ?';
      var insertDelete = ['activeSessions', 'client_ip', req.ip];
      sessionDeleteSql = mysql.format(sessionDeleteSql,insertDelete);
      // console.log("sessionDeleteSql is: " + sessionDeleteSql);
      connection.query(sessionDeleteSql, printSqlError);
      callback("NOT LOGGED IN");
      return;
    }
    //return active session
    else {
      // console.log("found a valid active session!");
      callback(null,rows[0]);
      return;
    }
  });
}

function getUsersByParams(req,callback) {

  var counter = 0;
  sql = 'SELECT ??,??,?? FROM ?? WHERE';
  sqlInsert = ['fname','lname','userId','users'];
  for (var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      // console.log(key + " -> " + req.body[key]);
      if(!isEmpty(req.body[key])){
        counter++;
        // console.log("non-empty:" + key + " -> " + req.body[key]);
        sql += ' ?? LIKE ? AND';
        sqlInsert.push(key);
        sqlInsert.push('%' + req.body[key] + '%');
      }
    }
  }

  //if no params to filter by
  if (counter == 0) {
    //trim the 'where'
    sql = sql.substring(0,sql.length-5);
  } else {
    //trim the 'and'
    sql = sql.substring(0,sql.length-3);
  }


  sql = mysql.format(sql,sqlInsert);

  // console.log("sql: " + sql);
  connection.query(sql,function getSqlUsers(err,rows,fields) {
    if(err) {callback(err);return;}

    if(rows.length == 0) {
      callback("NO MATCHING USERS FOUND");
      return;
    }
    // console.log("found some rows! returning them...");
    callback(null,rows);
  }
  );

}























//checks results of state pair query
// function querySqlForStateCode(req, callback) {
//   //get stateCode/state pair
//   var stateSql = 'SELECT * FROM ?? WHERE (?? = ? OR ?? = ?)';
//   var stateInserts = ['stateCodes','stateCode',req.body.state,'state',req.body.state];
//   stateSql = mysql.format(stateSql,stateInserts);
//   connection.query(stateSql,function getStatePair (err,rows,fields) { 
//     if (err) callback(err);
//     console.log("getStatePair");
//     //if state not in database
//     if(rows.length == 0) {
//       callback("INVALID STATE");
//       return;
//     }
//     //if state in database, no error and return rows
//     callback(null, rows);
//   });
// }




